let navbar = document.getElementById("navbar");
let selectedTheme = document.getElementById("selectedTheme");
let themeListToggler = document.getElementById("themeListToggler");
let customizationTab = document.getElementById("customization");
let navbarToggler = document.getElementById("navbarToggler");

navbarToggler.addEventListener('click', function navbarToggle(){
    if (navbar.style.display != "none"){
        navbar.style.display = "none";
        navbarToggler.style.opacity = "0";
    }
    else{
        navbar.style.display = "flex"
    };
});
navbarToggler.addEventListener('mouseover', function show(){
    navbarToggler.style.opacity = "1"
})
navbarToggler.addEventListener('mouseout', function show(){
    navbarToggler.style.opacity = "0"
})
// .themeListToggler