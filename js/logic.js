let minuteHand = document.getElementById("minuteHand");
let secondHand = document.getElementById("secondHand");
let hourHand = document.getElementById("hourHand");
let date = document.getElementById("date");
let month = document.getElementById("month");
let year = document.getElementById("year");
function displayTime(){
    time = new Date();
    hourHand.innerHTML = time.getHours();
    minuteHand.innerHTML = time.getMinutes();
    secondHand.innerHTML = time.getSeconds();
    date.innerHTML = time.getDate()
    month.innerHTML = time.getMonth()
    year.innerHTML = time.getFullYear()
};
setInterval(displayTime, 1000);